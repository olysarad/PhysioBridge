# PhysioBridge

PhysioBridge is an application developed during a Masters thesis, which purpose was to investigate the possibility of using virtual reality for the sensorimotor walkway exercise. The system consists of two applications, a VR application developed for Oculus Quest 2 and an Android mobile application for controlling the VR experience.


### HW requirements
- Oculus Quest 2 or 3
- Android smartphone with version 10 or higher
- Wi-Fi connection (both applications need to be on the same network)


### SW requirements
- Unity LTS version 2022.3.11f1 or higher


### Application build
Both the VR application and the control Android application were developed as a single project in Unity. Once this repository is cloned and opened in the Unity editor, both of the applications can be build onto the desired device. Due to building on different target devices, it is important to use various settings:

#### VR application
- **File &#8594; Build Settings :** VRLobby scene must be checked and MobileLobby unchecked
- **Edit &#8594; Project Settings &#8594; XR Plug-in Management :** Oculus must be checked in the Android tab

#### Android application
- **File &#8594; Build Settings :** MobileLobby scene must be checked and VRLobby unchecked
- **Edit &#8594; Project Settings &#8594; XR Plug-in Management :** Oculus must be unchecked in the Android tab
- **Edit &#8594; Project Settings &#8594; Player &#8594; :** Under Resolution and Presentation is option for default orienetation, which should be set to Auto rotate 
