﻿using Oculus.Interaction;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class AnchorManager : MonoBehaviour    // TODO: Fix preview rotation + first select (when starting with select mode) bug
{  
    /// <summary>
    /// Anchor Mode switches between create and select
    /// </summary>
    public enum AnchorMode
    {
        Create,
        Select
    };

    [SerializeField] private GameObject debugRemoveAnchorPose;

    [SerializeField] private Transform raycasterPinchVisualTransform;
    [SerializeField] private AnchorLoader anchorLoader;
    [SerializeField] private Anchor anchorPrefab;
    [SerializeField] private GameObject anchorPreviewPrefab;

    private delegate void PrimaryPressDelegate();
    private PrimaryPressDelegate _primaryPressDelegate;

    private Anchor anchor;
    private GameObject anchorPreview;

    private AnchorMode mode = AnchorMode.Select;

    UnityEngine.Events.UnityAction removeAnchorCall;

    private void OnEnable()
    {
        anchorPreview = Instantiate(anchorPreviewPrefab, anchorPreviewPrefab.transform.position, anchorPreviewPrefab.transform.rotation);

        // Gesture for removing anchor from plaayer prefs - meant mostly for bugs and debugging
        removeAnchorCall = () =>
        {
            Debug.Log("Erase gesture detected");
            RemoveAnchor();
            Debug.Log("Anchor erased");
        };
        debugRemoveAnchorPose.GetComponent<SelectorUnityEventWrapper>().WhenSelected.AddListener(removeAnchorCall);

        // Start the appropriate mode based on saved anchor
        if (PlayerPrefs.HasKey(Constants.anchorUuidPlayerPref))
        {
            anchor = FindObjectOfType<Anchor>();

            if (anchor == null)
            {
                Debug.Log("Player prefs SET but NO anchor loaded - deleting player prefs");

                PlayerPrefs.DeleteKey(Constants.anchorUuidPlayerPref);
                mode = AnchorMode.Create;
                StartCreateMode();
            }
            else {
                anchor.GetComponent<InteractableUnityEventWrapper>().WhenUnselect.RemoveAllListeners();
                anchor.GetComponent<InteractableUnityEventWrapper>().WhenUnselect.AddListener(() => {
                    _primaryPressDelegate?.Invoke();
                });

                mode = AnchorMode.Select;
                StartSelectMode();
            }
        }
        else {
            mode = AnchorMode.Create;
            StartCreateMode();
        }
    }

    private void OnDisable()
    {
        debugRemoveAnchorPose.GetComponent<SelectorUnityEventWrapper>().WhenSelected.RemoveListener(removeAnchorCall);
        Destroy(anchorPreview.gameObject);
    }


    private void Update()
    {
        if (mode == AnchorMode.Create)
        {
            // Handling placement preview movement
            ControlPreviewPosition();

            if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RHand))
            {
                _primaryPressDelegate?.Invoke();
            }
        }
    }

    #region CREATE MODE
    /*
     * During CREATE mode the anchor placement preview is shown in front of the right controller. 
     * 
     * When an action is triggered :
     *  - Placement preview is hidden
     *  - New anchor is instantiated (and saved in external storage)
     *  - Mode is changed to SELECT mode
    */

    /******************* Handling mode change *****************/
    private void StartCreateMode()
    {
        anchorPreview.gameObject.SetActive(true);
        _primaryPressDelegate = CreateAnchor;
    }

    /******************* Handling Anchor movement *****************/
    private void ControlPreviewPosition()
    {
        // Disable rotation in x and z axis
        anchorPreview.transform.position = raycasterPinchVisualTransform.position + new Vector3(raycasterPinchVisualTransform.forward.x, 0, raycasterPinchVisualTransform.forward.z) * .1f;
        anchorPreview.transform.eulerAngles = new Vector3(0, raycasterPinchVisualTransform.eulerAngles.y, 0);
    }

    /******************* Delegate method *****************/

    private void CreateAnchor()
    {
        anchor = Instantiate(anchorPrefab, anchorPreview.transform.position, anchorPreview.transform.rotation);
        anchor.GetComponent<InteractableUnityEventWrapper>().WhenUnselect.AddListener(() => { _primaryPressDelegate?.Invoke(); });

        ToggleModes();
    }


    #endregion

    #region SELECT MODE
    /*
     * During SELECT mode ray used for anchor hover/select is casted from the right controller. 
     * 
     * When anchor is hovered by the casted ray and an action is triggered :
     *  - Hovered anchor is destroyed (and removed from the external storage)
     *  - The mode is switched back to CREATE mode
    */

    /******************* Handling mode change *****************/

    private void StartSelectMode()
    {
        anchorPreview.gameObject.SetActive(false);
        anchor.ShowMesh();
        _primaryPressDelegate = RemoveAnchor;
    }

    /******************* Delegate Method *****************/

    private void RemoveAnchor()
    {
        if (anchor == null) {
            Debug.Log("NO ANCHOR ERR : Player prefs has key = " + PlayerPrefs.HasKey(Constants.anchorUuidPlayerPref));
            return;
        }
            
        anchor.EraseAnchor();

        Destroy(anchor.gameObject);

        ToggleModes();
    }
    #endregion

    #region MODE SWITCHING
    /******************* Change between modes Methods *****************/

    private void ToggleModes()
    {
        if (mode == AnchorMode.Select)
        {
            mode = AnchorMode.Create;
            StartCreateMode();
        }
        else
        {
            mode = AnchorMode.Select;
            StartSelectMode();
        }
    }

    #endregion
}
