using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AnchorLoader : MonoBehaviour
{
    [SerializeField] OVRSpatialAnchor anchorPrefab;

    Action<bool, OVRSpatialAnchor.UnboundAnchor> _onAnchorLocalized;

    public event EventHandler OnAnchorLoaded;

    private void Awake()
    {
        _onAnchorLocalized = OnLocalized;
    }

    private void OnLocalized(bool success, OVRSpatialAnchor.UnboundAnchor unboundAnchor)
    {
        if (!success) return;

        var pose = unboundAnchor.Pose;
        var spatialAnchor = Instantiate(anchorPrefab, pose.position, pose.rotation);
        unboundAnchor.BindTo(spatialAnchor);

        OnAnchorLoaded?.Invoke(this, EventArgs.Empty);
    }

    private void Load(OVRSpatialAnchor.LoadOptions options) => OVRSpatialAnchor.LoadUnboundAnchorsAsync(options)
    .ContinueWith(anchors =>
    {
        if (anchors == null || anchors.Length == 0) {
            Debug.Log("No anchors found on load");
            PlayerPrefs.DeleteKey(Constants.anchorUuidPlayerPref);
            return;
        }
        
        foreach (var anchor in anchors)
        {
            if (anchor.Localized)
            {
                _onAnchorLocalized(true, anchor);
            }
            else if (!anchor.Localizing)
            {
                anchor.LocalizeAsync().ContinueWith(_onAnchorLocalized, anchor);
            }
        }
    });

    public void LoadAnchorsByUuid()
    {
        // Get number of saved anchor uuids
        if (!PlayerPrefs.HasKey(Constants.anchorUuidPlayerPref)) return;

        Guid[] uuids = { new Guid(PlayerPrefs.GetString(Constants.anchorUuidPlayerPref)) };

        Load(new OVRSpatialAnchor.LoadOptions
        {
            Timeout = 0,
            StorageLocation = OVRSpace.StorageLocation.Local,
            Uuids = uuids
        });
    }
}
