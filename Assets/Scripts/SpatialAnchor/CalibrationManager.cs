using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalibrationManager : MonoBehaviour
{
    [Header("Passthrough switch")]
    [SerializeField] private OVRPassthroughLayer passthrough;
    [SerializeField] private Material skybox;

    [Header("Hand poses")]
    [SerializeField] private GameObject endHandPose;

    [Header("Logic elements")]
    [SerializeField] private AnchorManager anchorManager;
    [SerializeField] private Bridge bridgeNetworkPrefab;

    [Header("UI elements")]
    [SerializeField] private MainMenuUI mainMenuUI;
    [SerializeField] private FadeScreen fadeScreen;
    [SerializeField] private List<GameObject> gameObjectsToBeHidden;


    public event EventHandler OnCalibrationEnded;

    public void StartCalibration() {
        StartCoroutine(StartCalibrationCoroutine());
    }
    public void EndCalibration()
    {
        StartCoroutine(EndCalibrationCoroutine());
    }


    IEnumerator StartCalibrationCoroutine() {
        Debug.Log("Starting calibration ...");
        fadeScreen.FadeIn();
        yield return new WaitForSecondsRealtime(fadeScreen.fadeDuration);

        Bridge bridge = FindObjectOfType<Bridge>();
        if (bridge != null)
            NetworkServer.UnSpawn(bridge.gameObject);

        anchorManager.enabled = true;
        endHandPose.SetActive(true);
        gameObjectsToBeHidden.ForEach(element => element.gameObject.SetActive(false));
        passthrough.enabled = true;
        RenderSettings.skybox = null;

        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            player.CmdChangeVRState(VRState.Calibration);
        }

        yield return new WaitForSecondsRealtime(1);
        fadeScreen.FadeOut();
    }

    IEnumerator EndCalibrationCoroutine()
    {
        Debug.Log("Ending calibration ...");
        fadeScreen.FadeIn();
        yield return new WaitForSecondsRealtime(fadeScreen.fadeDuration);

        anchorManager.enabled = false;
        endHandPose.SetActive(false);
        gameObjectsToBeHidden.ForEach(element => element.gameObject.SetActive(true));
        passthrough.enabled = false;
        RenderSettings.skybox = skybox;

        OnCalibrationEnded?.Invoke(this, EventArgs.Empty);

        VRState currentVRState = VRState.None;
        Anchor anchor = FindObjectOfType<Anchor>();
        if (anchor != null)
        {
            anchor.HideMesh();
            mainMenuUI.EnableGameStart();
            currentVRState = VRState.LobbyWithAnchor;
            NetworkServer.Spawn(Instantiate(bridgeNetworkPrefab.gameObject));
        }
        else {
            mainMenuUI.DisableGameStart();
            currentVRState = VRState.LobbyWithoutAnchor;
        }

        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            player.CmdChangeVRState(currentVRState);
        }

        yield return new WaitForSecondsRealtime(1);
        fadeScreen.FadeOut();
    }
}
