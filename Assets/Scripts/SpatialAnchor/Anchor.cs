using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(OVRSpatialAnchor))]
public class Anchor : MonoBehaviour
{
    [SerializeField] private GameObject mesh;

    [NonSerialized] public OVRSpatialAnchor spatialAnchor;


    private void Awake()
    {
        spatialAnchor = GetComponent<OVRSpatialAnchor>();
    }

    private IEnumerator Start()
    {
        // Keep checking for a valid and localized anchor state
        while (!spatialAnchor.Created && !spatialAnchor.Localized)
        {
            yield return new WaitForEndOfFrame();
        }


        // Save anchor externaly to Player Prefs
        spatialAnchor.SaveAsync().ContinueWith((success, anchor) =>
        {
            if (!success) return;

            SaveUuidToPlayerPrefs(anchor.spatialAnchor.Uuid);
        }, this);
    }

    public void EraseAnchor()
    {
        if (!spatialAnchor) return;

        spatialAnchor.EraseAsync().ContinueWith((success, anchor) =>
        {
            if (!success) return;

            RemoveUuidsFromPlayerPrefs();
        }, this);
    }

    private void SaveUuidToPlayerPrefs(Guid uuid)
    {
        Debug.Log("Anchor saveAsync saving");
        PlayerPrefs.SetString(Constants.anchorUuidPlayerPref, uuid.ToString());
        Debug.Log("Anchor saveAsync saving 2");
    }

    private void RemoveUuidsFromPlayerPrefs()
    {

        if (!PlayerPrefs.HasKey(Constants.anchorUuidPlayerPref))
        {
            return;
        }

        PlayerPrefs.DeleteKey(Constants.anchorUuidPlayerPref);
    }
    
    public void ShowMesh() {
        mesh.SetActive(true);
    }

    public void HideMesh() {
        mesh.SetActive(false);
    }
}
