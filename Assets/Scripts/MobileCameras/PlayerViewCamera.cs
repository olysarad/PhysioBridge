using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Script for updating camera position according to VR player
 */
public class PlayerViewCamera : MonoBehaviour
{
    private NetworkPlayer playerVR;

    private void Start()
    {
        // App assume only two network players - Oculus and Android mobile app
        // Local player == the mobile app network players 
        // The other is then VR player
        // TODO: Make syncVar for boolean if network player is VR? (might check on server add player and current scene)
        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            if (player.VRPlayer) 
                playerVR = player;
        }
    }
    private void Update()
    {
        if (playerVR) { 
            transform.position = playerVR.transform.position;
            transform.rotation = playerVR.transform.rotation;
        }
    }
}
