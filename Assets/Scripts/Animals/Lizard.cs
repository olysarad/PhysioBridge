using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lizard : AnimalController
{
    void Start()
    {
        float transitionStart = Random.Range(0, 20);   // Seconds until change transition start
        float transitionRepeat = Random.Range(30, 40); // Seconds for repeating transition change 

        InvokeRepeating("TriggerIdle", transitionStart, transitionRepeat);
    }
    void TriggerIdle()
    {
        _movement.StopMovement();
        _animator.SetTrigger("Idle");
    }


    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision triggered");
        if (collision.gameObject.name == "StopCollider")
        {
            _movement.StopMovement();
            _animator.SetTrigger("Idle");
        }
    }
}
