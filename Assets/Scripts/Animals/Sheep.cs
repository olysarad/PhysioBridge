using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 Sheep script: 
    Script for both static and dynamic (walking/running) sheep. Based on random parameter, the sheep change animations every randomly selected seconds. 
    The transitions have different probabilities described bellow:

    Static sheep:
        - 50% Eat animation
        - 50% Idle B animation

    Dynamic sheep:
        - 25% Eat animation
        - 25% Idle B animation
        - 50% Walk animation    (for *walkSeconds* amount of time)
 */

public class Sheep : AnimalController
{
    [SerializeField] private int transitionStart;
    [SerializeField] private int transitionRepeat;

    [SerializeField] private float walkSeconds = 20f;

    void Start()
    {
        transitionStart = Random.Range(0, 5);   // Seconds until change transition start
        transitionRepeat = Random.Range(10, 20); // Seconds for repeating transition change 

        InvokeRepeating("TriggerRandomTransition", transitionStart, transitionRepeat);
    }


    void TriggerRandomTransition()
    {
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("idle A")) return;

        float transitionParam = Random.Range(0f, 1f);

        if (_movement != null)  // Moving sheep
        {
            if (transitionParam < 0.25f)
            {
                _animator.SetTrigger("Eat");
            }
            else if (transitionParam < 0.5f)
            {
                _animator.SetTrigger("Idle B");
            }
            else
            {
                StartCoroutine(Walk());
            }
        }
        else    // Static sheep
        {
            if (transitionParam < 0.5f)
            {
                _animator.SetTrigger("Eat");
            }
            else
            {
                _animator.SetTrigger("Idle B");
            }
        }
    }


    IEnumerator Walk()
    {

        _animator.SetBool("Walking", true);
        _movement.TriggerWalkMovement();

        yield return new WaitForSeconds(walkSeconds);

        _animator.SetBool("Walking", false);
        _movement.StopMovement();
    }
}