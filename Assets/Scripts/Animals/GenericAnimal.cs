using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericAnimal : AnimalController
{
    [SerializeField] private string idleAnimationName;
    [SerializeField] private float movementProbability;
    
    [SerializeField] private bool canWalk;
    [SerializeField] private bool canRun;

    [Header("Debug elements")]
    [SerializeField] private int transitionStart;
    [SerializeField] private int transitionRepeat;
    [SerializeField] private float walkSeconds = 20f;
    [SerializeField] private float runSeconds = 3f;

    void Start()
    {
        transitionStart = Random.Range(0, 5);   // Seconds until change transition start
        transitionRepeat = Random.Range(10, 20); // Seconds for repeating transition change 

        InvokeRepeating("TriggerTransition", transitionStart, transitionRepeat);
    }

    void TriggerTransition() {
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsName(idleAnimationName)) return;

        if (_movement != null && Random.Range(0f, 1f) < movementProbability)
        {
            TriggerMovement();
        }
        else {
            _animator.SetTrigger("Trigger");
            _animator.SetFloat("TriggerParam", Random.Range(0f, 1f));
        }
    }

    void TriggerMovement() {
        if (canWalk && canRun)
        {
            if (Random.Range(0f, 1f) < .5)
                StartCoroutine(Walk());
            else
                StartCoroutine(Run());
        }
        else if (canWalk) {
            StartCoroutine(Walk());
        } else if (canRun)
        {
            StartCoroutine(Run());
        }
    }

    void PlaySound() {
        if (_audioSource != null)
            _audioSource.PlayOneShot(_audioSource.clip);
    }


    IEnumerator Walk()
    {

        _animator.SetBool("Walking", true);
        _movement.TriggerWalkMovement();

        yield return new WaitForSeconds(walkSeconds);

        _animator.SetBool("Walking", false);
        _movement.StopMovement();
    }

    IEnumerator Run()
    {

        _animator.SetBool("Running", true);
        _movement.TriggerRunMovement();

        yield return new WaitForSeconds(runSeconds);

        _animator.SetBool("Running", false);
        _movement.StopMovement();
    }
}
