using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 Dog script: 
    The dog barks every 60 seconds. In between the barks dog is idle doing the idling cycle animation 
    or is walking and running around predefided path. The walk cacle repeats after 5 - 10 seconds after 
    the previous walk ended.

 */

public class Dog : AnimalController
{
    private float walkTimer;
    private bool onWalk;

    void Start()
    {
        onWalk = false;
        walkTimer = Random.Range(5f, 10);

        InvokeRepeating("Bark", 60f, 60f);   // Bark every minute
    }

    private void Update()
    {
        walkTimer -= Time.deltaTime;

        if (walkTimer <= 0 && _animator.GetCurrentAnimatorStateInfo(0).IsName("idle A") && !onWalk) {
            GoForWalk();
        }
    }

    void GoForWalk()
    {
        onWalk = true;
        _animator.SetBool("WalkingAround", true);
        _movement.TriggerWalkMovement();
    }

    void Bark()
    {
        _movement.StopMovement();
        _animator.SetTrigger("Bark");
        _audioSource.PlayOneShot(_audioSource.clip);

        StartCoroutine(TriggerMovementAfterBarkCoroutine());
    }

    IEnumerator TriggerMovementAfterBarkCoroutine()
    {
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length + 1f);

        if (_animator.GetBool("WalkingAround")) 
        {
            _movement.TriggerWalkMovement();
        } else if (_animator.GetBool("RunningAround"))
        {
            _movement.TriggerRunMovement();
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "RunCollider")
        {
            _animator.SetBool("WalkingAround", false);
            _animator.SetBool("RunningAround", true);
            _movement.TriggerRunMovement();

        }

        if (collision.gameObject.name == "StopCollider")
        {
            onWalk = false;
            walkTimer = Random.Range(5f, 10f);

            _animator.SetBool("RunningAround", false);
            _movement.StopMovement();

        }
    }
}