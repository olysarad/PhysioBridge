using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 Cat script: 
    - Script for walking cat colliding with a watter bowl
    - Cat walks along predefined Bezier path and on collision drink and idle for some time
 */

public class Cat : AnimalController
{
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "DrinkCollider")
        {
            _animator.SetTrigger("Drink");
            _movement.StopMovement();

            StartCoroutine(IdleForRandomTime());
        }
    }

    IEnumerator IdleForRandomTime()
    {
        
        float waitingTime = Random.Range(5f, 30f);  // Waiting time ranges from 5 to 30 seconds

        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length + waitingTime);

        _animator.SetTrigger("Walk");
        _movement.TriggerWalkMovement();
    }
}