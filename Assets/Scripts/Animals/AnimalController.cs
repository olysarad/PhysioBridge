using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using Unity.IO.LowLevel.Unsafe;
using UnityEngine;

public class AnimalController : MonoBehaviour
{
    protected Animator _animator;
    protected PathFollower _movement;
    protected AudioSource _audioSource;


    void Awake()
    {
        _animator = GetComponent<Animator>();
        _movement = GetComponent<PathFollower>();
        _audioSource = GetComponent<AudioSource>();
    }
}
