using Mirror;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Bridge : NetworkBehaviour
{
    [Header("Bridge handles")]
    [SerializeField] private GameObject leftHandle;
    [SerializeField] private GameObject rightHandle;

    [SerializeField] public HandleCollisionDetection leftHandleCollider;
    [SerializeField] public HandleCollisionDetection rightHandleCollider;

    [Header("Missing planks")]
    [SerializeField] private List<GameObject> planksToBeHidden;

    [Header("Rugs")]
    [SerializeField] private GameObject rugs;
    [SerializeField] private GameObject firstRugSwitch;
    [SerializeField] private GameObject secondRugSwitch;

    [Header("SyncVars")]
    [SyncVar(hook = "HidePlanks")] public bool missingPlanks = false;

    // [SyncVar(hook = "SwitchRugs")] public bool switchedRugs = false;
    [SyncVar(hook = "UpdateRugsPreview")] public RugsPreviewType rugsPreview = RugsPreviewType.DefaultRugs;

    [SyncVar(hook = "UpdateHandles")] public BridgeHandleType handleType = BridgeHandleType.BothHandles;

    private Anchor anchor;
    private Vector3 firstRugPosition;
    private Vector3 secondRugPosition;

    private void Start()
    {
        anchor = FindObjectOfType<Anchor>();

        firstRugPosition = firstRugSwitch.transform.localPosition;
        secondRugPosition = secondRugSwitch.transform.localPosition;
    }

    private void Update()
    {
        if (anchor != null)
        {
            this.transform.position = anchor.transform.position;
            this.transform.rotation = anchor.transform.rotation;
        }
    }

    #region ----- CALLBACKS -----
    public void HidePlanks(bool oldValue, bool hide) {
        planksToBeHidden.ForEach(plank => plank.SetActive(!hide));   
    }

    public void UpdateRugsPreview(RugsPreviewType oldValue, RugsPreviewType type) {
        Debug.Log("Update rugs preview -> " + type);
        switch (type)
        {
            case RugsPreviewType.DefaultRugs:
                firstRugSwitch.transform.localPosition = firstRugPosition;
                secondRugSwitch.transform.localPosition = secondRugPosition;
                rugs.SetActive(true);
                break;
            case RugsPreviewType.SwitchedRugs:
                firstRugSwitch.transform.localPosition = secondRugPosition;
                secondRugSwitch.transform.localPosition = firstRugPosition;
                rugs.SetActive(true);
                break;
            case RugsPreviewType.NoRugs:
                rugs.SetActive(false);
                break;
        }
    }

    public void UpdateHandles(BridgeHandleType oldValue, BridgeHandleType type) {
        switch (type)
        {
            case BridgeHandleType.BothHandles:
                leftHandle.SetActive(true);
                rightHandle.SetActive(true);
                break;
            case BridgeHandleType.LeftHandle:
                leftHandle.SetActive(true);
                rightHandle.SetActive(false);
                break;
            case BridgeHandleType.RightHandle:
                leftHandle.SetActive(false);
                rightHandle.SetActive(true);
                break;
            case BridgeHandleType.NoHandle:
                leftHandle.SetActive(false);
                rightHandle.SetActive(false);
                break;
        }
    }
    #endregion


    #region ----- COMMANDS -----
    /*[Command(requiresAuthority = false)]
    public void CmdSetSwitchedRugs(bool value)
    {
        switchedRugs = value;
    }*/

    [Command(requiresAuthority = false)]
    public void CmdSetRugsPreviewType(RugsPreviewType value)
    {
        rugsPreview = value;
    }

    [Command(requiresAuthority = false)]
    public void CmdSetHidePlanks(bool value)
    {
        missingPlanks = value;
    }

    [Command(requiresAuthority = false)]
    public void CmdSetHandleType(BridgeHandleType value)
    {
        handleType = value;
    }
    #endregion
}
