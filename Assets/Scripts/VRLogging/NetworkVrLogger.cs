using Mirror;
using Oculus.Interaction.Samples;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;
using VrDashboardLogger.Editor;
using VrDashboardLogger.Editor.Classes;

public class NetworkVrLogger : NetworkBehaviour
{
    [SerializeField] private string organizationIdentifier;

    [Header("SyncVars")]
    [SyncVar(hook = "OnEmptyParticipantResetUI")] 
    public Participant participantToLog;

    [SyncVar(hook = "HandleParticipantsUpdate")]
    public List<Participant> participants;

    public bool loggingInProgress = false;

    private VrLogger vrLogger;

    public void SetCustomData(RugsPreviewType rugsType, bool missingPlanks, BridgeHandleType handleType)
    {
        string json = "{" +
                "\"RugsPreviewType\": \"" + rugsType.ToString() + "\"," +
                "\"missingPlanks\": " + missingPlanks.ToString().ToLower() + "," +
                "\"handleType\": \"" + handleType.ToString() + "\"" +
            "}";
        Debug.Log(json);


        vrLogger.SetCustomData(json);
    }

    public void StartLogging() {
        if (participantToLog.id == string.Empty || participantToLog.id == null) return;

        vrLogger.SetParticipant(participantToLog.id);
        vrLogger.InitializeLogger();
        vrLogger.StartLogging(SceneManager.GetActiveScene().name);
        loggingInProgress = true;

        Debug.Log("Logging for " + participantToLog.nickname +" started");
    }

    public void StopLogging()
    {
        if (participantToLog.id == string.Empty || participantToLog.id == null) return;

        vrLogger.StopLogging();
        loggingInProgress = false;
        vrLogger.SendActivity(response =>
        {
            Debug.Log("VrLogger send acticity respose -> " + response);
        }, true);

        Debug.Log("Logging for " + participantToLog.nickname + " stopped");
    }

    public void SetEvent(string eventName)
    {
        vrLogger.SetEvent(eventName);
    }


    #region ----- CALLBACKS -----
    public void HandleParticipantsUpdate(List<Participant> oldParticipants, List<Participant> newParticipants)
    {
        VrLoggingUI vrLoggerUI = FindObjectOfType<VrLoggingUI>();
        if(vrLoggerUI == null) { return; }

        vrLoggerUI.ShowParticipants(newParticipants);
        vrLoggerUI.UpdateUIBasedOnServerData();
    }

    public void OnEmptyParticipantResetUI(Participant oldLogParticipant, Participant newLogParticipant)
    {
        if (newLogParticipant.id != string.Empty && newLogParticipant.id != null) { return; }

        VrLoggingUI vrLoggerUI = FindObjectOfType<VrLoggingUI>();
        if (vrLoggerUI == null) { return; }

        vrLoggerUI.UpdateUIBasedOnServerData();
        vrLoggerUI.OnParticipantPicked(0);

    }
    #endregion

    #region ----- COMMANDS -----
    [Command(requiresAuthority = false)]
    public void CmdSetParticipantToLog(Participant participant)
    {
        participantToLog = participant;
    }

    // [Command(requiresAuthority = false)]
    public void CmdUpdateParticipants()
    {
        vrLogger = GetComponent<VrLogger>();

        vrLogger.SetOrganisation(organizationIdentifier);

        vrLogger.GetParticipants(list =>
        {
            participants = list;
        });
    }
    #endregion
}
