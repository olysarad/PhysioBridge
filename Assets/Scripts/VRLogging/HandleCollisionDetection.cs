using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HandleCollisionDetection : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.parent.name == Constants.colliderName) {
            Debug.Log(this.name + " Grab enter");
            NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();

            if (vrLogger != null && vrLogger.loggingInProgress) {
                string eventKey = this.name + "Grab";

                Debug.Log("Sending event with key " + eventKey);
                vrLogger.SetEvent(eventKey);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.transform.parent.name == Constants.colliderName) {
            Debug.Log(this.name + " Grab exit");
            NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();

            if (vrLogger != null && vrLogger.loggingInProgress) {
                string eventKey = this.name + "Release";

                Debug.Log("Sending event with key " + eventKey);
                vrLogger.SetEvent(eventKey);
            }
        } 
    }
}
