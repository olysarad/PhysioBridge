using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VRState
{
    LobbyWithoutAnchor,
    LobbyWithAnchor,
    Calibration,
    Exercise,
    None
}
