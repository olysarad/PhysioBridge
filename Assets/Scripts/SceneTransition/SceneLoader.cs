using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class SceneLoader : MonoBehaviour
{
    private FadeScreen fader;
    private bool processing = false;

    // EventHandlers
    public event EventHandler OnSceneLoaded;
    public event EventHandler OnSceneUnloaded;

    public void LoadSceneAdditive(string sceneName)
    {
        if (!processing) 
            LoadScene(sceneName, LoadSceneMode.Additive);
    }

    public void UnloadActiveScene()
    {
        if (SceneManager.loadedSceneCount > 1 && !processing) { 
            StartCoroutine(UnloadSceneAsync());
        }
    }


    #region PRIVATE
    private void LoadScene(string sceneName, LoadSceneMode mode)
    {
        fader = FindObjectOfType<FadeScreen>();
        StartCoroutine(LoadSceneAsync(sceneName, mode));
    }


    IEnumerator LoadSceneAsync(string sceneName, LoadSceneMode mode)
    {
        processing = true;
        // Fade scene out
        if (fader != null)
        {
            fader.FadeIn();
            yield return new WaitForSecondsRealtime(fader.fadeDuration);
        }

        // Load game scene
        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(sceneName, mode);

        while (!loadOperation.isDone)
        {
            yield return new WaitForSecondsRealtime(0.05f);
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));

        yield return new WaitForSecondsRealtime(1);

        OnSceneLoaded?.Invoke(this, EventArgs.Empty);

        // Fade in again
        if (fader != null) fader.FadeOut();
        processing = false;
    }

    IEnumerator UnloadSceneAsync()
    {
        processing = true;
        // Fade scene out
        if (fader != null)
        {
            fader.FadeIn();
            yield return new WaitForSecondsRealtime(fader.fadeDuration);
        }

        // Unload current scene
        AsyncOperation unloadOperation = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        while (!unloadOperation.isDone)
        {
            yield return new WaitForSecondsRealtime(0.05f);
        }

        OnSceneUnloaded?.Invoke(this, EventArgs.Empty);

        // Fade in again
        if (fader != null) fader.FadeOut();
        processing = false;
    }

    #endregion
}
