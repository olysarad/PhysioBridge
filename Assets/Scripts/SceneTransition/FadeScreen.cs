using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeScreen : MonoBehaviour
{
    [SerializeField] public float fadeDuration = 2;
    [SerializeField] private Color fadeColor;

    [SerializeField] private Canvas loadingCanvas;

    private Renderer rend;


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        FadeOut();
    }

    public void FadeIn()
    {
        StartCoroutine(FadeRoutine(0, 1));
    }

    public void FadeOut()
    {
        loadingCanvas.gameObject.SetActive(false);
        StartCoroutine(FadeRoutine(1, 0));
    }


    public IEnumerator FadeRoutine(float alphaIn, float alphaOut)
    {
        float timer = 0;
        while (timer < fadeDuration)
        {
            Color newColor = fadeColor;
            newColor.a = Mathf.Lerp(alphaIn, alphaOut, timer / fadeDuration);

            rend.material.SetColor("_BaseColor", newColor);

            timer += Time.deltaTime;
            yield return null;
        }

        Color alphaOutColor = fadeColor;
        alphaOutColor.a = alphaOut;

        rend.material.SetColor("_BaseColor", alphaOutColor);

        loadingCanvas.gameObject.SetActive(alphaIn == 0);
    }
}
