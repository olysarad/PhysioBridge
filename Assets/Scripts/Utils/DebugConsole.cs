using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugConsole : MonoBehaviour
{
    public TextMeshProUGUI debugText;
    string output = "";

    private int outputLines = 0;
    [SerializeField] private int maxLines = 20;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
        Debug.Log("Log Enabled!");
    }

    private void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
        Clearlog();
    }

    private void HandleLog(string logString, string stackTree, LogType type)
    {
        if (outputLines > maxLines) { Clearlog(); }

        output = logString + "\n" + output;
        outputLines += 1;
    }

    private void Clearlog()
    {
        outputLines = 0;
        output = "";
    }
    private void OnGUI()
    {
        debugText.text = output;
    }
}
