using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string colliderName = "GripPoint";

    public const string anchorUuidPlayerPref = "anchorUuid";
 
    // ----- SCENE NAMES -----
    public const string VRLobbyScene = "VRLobby";
    public const string mobileLobbyScene = "MobileLobby";

    public const string countrysideScene = "Countryside";
    public const string desertScene = "Desert";
}

