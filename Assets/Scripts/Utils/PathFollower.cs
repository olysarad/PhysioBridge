using Oculus.Interaction;
using PathCreation;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    [SerializeField] private PathCreator pathCreator;
    [SerializeField] private EndOfPathInstruction endOfPathInstruction;
    [SerializeField] private float walkingSpeed;
    [SerializeField] private float runningSpeed;


    [SerializeField] private float speed = 0;
    private float distanceTravelled;

    void Update()
    {
        if (pathCreator != null)
        {
            distanceTravelled += speed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
            transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
        }
    }

    public void TriggerWalkMovement()
    {
        speed = walkingSpeed;
    }

    public void TriggerRunMovement()
    {
        speed = runningSpeed;
    }

    public void StopMovement()
    {
        speed = 0;
    }

    public float getSpeed()
    {
        return speed;  
    }
}
