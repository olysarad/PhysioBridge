using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Script updating Mirror network player position according to OVR camera for valid sync through the network
public class VRPlayerRig : MonoBehaviour
{
    [SerializeField] private Transform centeryEyeAnchor;

    [NonSerialized] public NetworkPlayer networkPlayer;

    public static event EventHandler OnVRPlayerRigCreated;

    private void Start()
    {
        OnVRPlayerRigCreated?.Invoke(this, EventArgs.Empty);
    }

    private void Update()
    {
        if (networkPlayer)
        {
            networkPlayer.transform.position = centeryEyeAnchor.transform.position;
            networkPlayer.transform.rotation = centeryEyeAnchor.transform.rotation;
        }
    }
}
