using Mirror;
using Mirror.Discovery;
using System;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * NetworkManager handles the connections and disconnections on Server/Client side of the network.
 */

public class CustomNetworkManager : NetworkManager
{

    // Event handlers
    public event EventHandler OnClientDisconnectEvent;
    public event EventHandler OnVRPlayerConnected;
    public event EventHandler OnMobilePlayerConnected;
    public event EventHandler OnServerDisconnectEvent;

    public override void Awake()
    {
        if (FindObjectsOfType<CustomNetworkManager>().Length > 1) { 
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        base.Awake();
    }


    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);

        NetworkPlayer player = conn.identity.GetComponent<NetworkPlayer>();

        if (player.isLocalPlayer && OVRManager.isHmdPresent)
        {
            player.CmdSetVRPlayer();
            OnVRPlayerConnected?.Invoke(this, new EventArgs());
        }
        else {
            OnMobilePlayerConnected?.Invoke(this, new EventArgs());
        }

        Debug.Log("PLAYER ADD");
    }


    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        base.OnServerDisconnect(conn);

        OnServerDisconnectEvent?.Invoke(this, new EventArgs());

        Debug.Log("SRVR DISCO");
    }

    public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        base.OnServerConnect(conn);

        Debug.Log("SRVR CON");
    }

    public override void OnClientDisconnect()
    {
        base.OnClientDisconnect();

        OnClientDisconnectEvent?.Invoke(this, new EventArgs());
        Debug.Log("CLI DISCO");
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();

        Debug.Log("Cli conn - " + FindObjectsOfType<NetworkPlayer>().Length);

        Debug.Log("CLI CONN");
    }
}
