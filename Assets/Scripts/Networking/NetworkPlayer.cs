using Mirror;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

/**
 * Network Player stores the state of a player connected to the Mirror server. 
 * 
 * The Player is based on Commands and Callbacks functions. Where commands functions 
 * are used to change the value of SyncVars which after change trigger predefined callbacks.
 * 
 * Note: The application assume maximum of two players where one is the Oculus with the VR application (Host)
 * and the other is mobile application
 */

public class NetworkPlayer : NetworkBehaviour
{

    [SyncVar] public bool VRPlayer = false;

    [SyncVar(hook = "SetRandomSeed")] public int randomSeed = 42;

    [SyncVar(hook = "LoadWorld")] public string chosenWorld;

    [SyncVar(hook = "HandleVRStateChange")] public VRState VRState = VRState.None;


    private SceneLoader sceneLoader;

    private void Awake()
    {
        VRPlayerRig.OnVRPlayerRigCreated += OnVRPlayerRigCreated;

        sceneLoader = FindObjectOfType<SceneLoader>();

        DontDestroyOnLoad(this.gameObject);
    }

    #region ----- OVR RIG -----
    public override void OnStartLocalPlayer()
    {
        UpdateNetworkPlayerInVRPlayerRig();
    }

    private void OnVRPlayerRigCreated(object sender, EventArgs e)
    {
        UpdateNetworkPlayerInVRPlayerRig();
    }

    /**
     * VRPlayerRig controls the position and rotation of Network Player. 
     * This function sets Network Player in active VRPlayerRig.
     */
    private void UpdateNetworkPlayerInVRPlayerRig() {
        VRPlayerRig vrPlayerRig = GameObject.FindObjectOfType<VRPlayerRig>();

        if (vrPlayerRig != null)
        {
            vrPlayerRig.networkPlayer = this;
        }
    }
    #endregion


    #region ----- CALLBACKS -----
    // Callbacks are called whenever a specific SyncVar is changed

    /**
    * Trigger loading a scene (additively)
    */
    public void LoadWorld(string oldScene, string newScene)
    {
        if (String.IsNullOrEmpty(newScene)) return;

        if (this.isLocalPlayer)
        {
            Debug.Log("Start world -> " + newScene);
            sceneLoader.LoadSceneAdditive(newScene);
        }
    }

    public void HandleVRStateChange(VRState oldState, VRState newState)
    {
        MobileLobbyUI mobileLobbyUI = FindObjectOfType<MobileLobbyUI>();

        if (this.VRPlayer && mobileLobbyUI != null) {
            Debug.Log("UPDATE " + newState);
            mobileLobbyUI.OnVRStateChange(newState);
        }
    }

    public void SetRandomSeed(int oldValue, int newValue)
    {
        Debug.Log("RandomSeed initialized");
        Random.InitState(newValue);
    }
    #endregion


    #region ----- COMMANDS -----
    /**
     * Commands change the values of SyncVars 
     * - called from clients but executed on the server
     * - requiresAuthority set to false to be able to sync SyncVars across all players  
     */
    [Command(requiresAuthority = false)]
    public void CmdSetVRPlayer()
    {
        VRPlayer = true;
    }

    [Command(requiresAuthority = false)]
    public void CmdRandomSeed(int seed)
    {
        randomSeed = seed;
    }

    [Command(requiresAuthority = false)]
    public void CmdHandleSelectedWorld(string sceneName)
    {
        chosenWorld = sceneName;
    }

    [Command(requiresAuthority = false)]
    public void CmdChangeVRState(VRState state)
    {
        if (this.isLocalPlayer) { Debug.Log("VRState changed to " + state); }

        VRState = state;
    }

    [Command(requiresAuthority = false)]
    public void CmdGoToLobby()
    {
        GoToLobbyRpc();
    }
    #endregion


    #region ----- RPC -----
    /**
     * Remote Procedure Calls are called on the server and run on corresponding clients. 
     */
    [TargetRpc]
    public void GoToLobbyRpc()
    {
        if (this.isLocalPlayer)
        {
            Debug.Log("Going back to lobby");
            sceneLoader.UnloadActiveScene();
        }
    }
    #endregion
}
