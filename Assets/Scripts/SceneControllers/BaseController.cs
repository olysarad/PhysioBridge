using Mirror.Discovery;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class BaseController : MonoBehaviour
{
    protected NetworkDiscovery networkDiscovery;
    protected CustomNetworkManager networkManager;

    private void Awake()
    {
        networkDiscovery = FindObjectOfType<NetworkDiscovery>();
        networkManager = FindObjectOfType<CustomNetworkManager>();
    }

    /**
     * Handles selecting a scene and triggers network synchronisation with 
     * Command CmdHandleSelectedWorld on all NetworkPlayers in the scene
     */
    public virtual void OnSceneSelected(string sceneName)
    {
        int randomSeed = Random.Range(10, 90);

        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            player.CmdRandomSeed(randomSeed);
            player.CmdHandleSelectedWorld(sceneName);
            player.CmdChangeVRState(VRState.Exercise);
        }
    }

    /**
     * Handles returning player to lobby from the exercise and triggers network synchronisation with 
     * Command CmdHandleSelectedWorld on all NetworkPlayers in the scene
     */
    public virtual void OnGoToLobby()
    {
        if (FindObjectOfType<NetworkPlayer>().VRState == VRState.Calibration) return;

        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            player.CmdGoToLobby();
            player.CmdHandleSelectedWorld(String.Empty);
            player.CmdChangeVRState(VRState.LobbyWithAnchor);
        }
    }

    public void OnQuit()
    {
        // TODO: make as SyncVar too? 
        Debug.Log("Quitting app");
        Application.Quit();
    }
}
