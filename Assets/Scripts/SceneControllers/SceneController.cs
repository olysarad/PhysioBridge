using Mirror;
using Oculus.Interaction;
using Oculus.Platform;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField] private Transform startingPoint;

    [SerializeField] private GameObject mobileCameras;

    void Start()
    {
        if (OVRManager.isHmdPresent)
        {
            // In VR app:
            //  - Move player to startion position in scene
            //  - Move bridge to starting position
            StartCoroutine(MovePlayerToStartingPosition());
        }
        else {
            // In mobile app:
            //  - Activate mobileCameras
            //  - Add bridge to scene
            mobileCameras.SetActive(true);
            // bridge.SetActive(true);
        }
    }

    IEnumerator MovePlayerToStartingPosition()
    {
        OVRCameraRig ovrCamera = FindObjectOfType<OVRCameraRig>();
        Anchor anchor = FindObjectOfType<Anchor>();

        ovrCamera.transform.Rotate(0, startingPoint.eulerAngles.y - anchor.transform.eulerAngles.y, 0);

        yield return new WaitForSecondsRealtime(.1f);

        ovrCamera.transform.position += new Vector3(
            startingPoint.position.x - anchor.transform.position.x,
            0,
            startingPoint.position.z - anchor.transform.position.z);
    }
}
