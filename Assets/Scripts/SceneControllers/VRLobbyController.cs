using Mirror.Discovery;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using VrDashboardLogger.Editor;
using VrDashboardLogger.Editor.Classes;

public class VRLobbyController : BaseController
{
    [SerializeField] private CalibrationManager calibrationManager;

    [Header("Network spawnable objects")]
    [SerializeField] private Bridge bridgeNetworkPrefab;
    [SerializeField] private VrLogger vrLoggerPrefab;

    [Header("VR Player")]
    [SerializeField] private GameObject head;
    [SerializeField] private GameObject leftHand;
    [SerializeField] private GameObject rightHand;

    [Header("UI")]
    [SerializeField] private MainMenuUI mainMenuUI;
    [SerializeField] private GameObject sceneSelectionUI;

    [Header("Loaders")]
    [SerializeField] private SceneLoader sceneLoader;
    [SerializeField] private AnchorLoader anchorLoader;


    private Participant emptyParticipant = new Participant();

    private void Start()
    {
        emptyParticipant.id = string.Empty;

        // Start server for network communication
        networkManager.StartHost();
        networkDiscovery.secretHandshake = 5630237;
        networkDiscovery.AdvertiseServer();
        networkManager.Start();

        // Handle anchor and UI 
        mainMenuUI.DisableGameStart();
        HandleAnchorLoading();

        // Event listeners
        anchorLoader.OnAnchorLoaded += OnAnchorLoaded;
        sceneLoader.OnSceneLoaded += OnSceneLoaded;
        sceneLoader.OnSceneUnloaded += OnSceneUnloaded;
        calibrationManager.OnCalibrationEnded += OnCalibrationEnded;
        networkManager.OnVRPlayerConnected += OnVRPlayerConnected;
        networkManager.OnMobilePlayerConnected += OnMobilePlayerConnected;
        networkManager.OnServerDisconnectEvent += OnServerDisconnectEvent;
    }


    #region EventHandlers
    private void OnVRPlayerConnected(object sender, System.EventArgs e)
    {
        mainMenuUI.setVRIndicatorActive(true);

        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            VRState state = FindObjectOfType<Anchor>() == null ? VRState.LobbyWithoutAnchor : VRState.LobbyWithAnchor;
            player.CmdChangeVRState(state);
        }
    }

    private void OnSceneLoaded(object sender, System.EventArgs e)
    {
        NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();
        Bridge bridge = FindObjectOfType<Bridge>();

        if (vrLogger != null && vrLogger.participantToLog.id != string.Empty) {
            vrLogger.SetCustomData(bridge.rugsPreview, bridge.missingPlanks, bridge.handleType);
            vrLogger.StartLogging();
        }
    }

    private void OnSceneUnloaded(object sender, System.EventArgs e)
    {
        NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();
        if (vrLogger != null && vrLogger.participantToLog.id != string.Empty) {
            vrLogger.StopLogging();
            vrLogger.CmdSetParticipantToLog(emptyParticipant);
        }

        mainMenuUI.gameObject.SetActive(true);
        sceneSelectionUI.SetActive(false);

        StartCoroutine(ResetPlayerCameraTransform());
    }

    private void OnMobilePlayerConnected(object sender, System.EventArgs e)
    {
        mainMenuUI.setMobileIndicatorActive(true);

        if (FindObjectOfType<VrLogger>() == null) {
            GameObject networkLogger = Instantiate(vrLoggerPrefab.gameObject);
            VrLogger vrLogger = networkLogger.GetComponent<VrLogger>();
            vrLogger.head = head;
            vrLogger.leftHand = leftHand;
            vrLogger.rightHand = rightHand;

            networkLogger.GetComponent<NetworkVrLogger>().CmdUpdateParticipants();

            NetworkServer.Spawn(networkLogger);
        }
    }

    private void OnServerDisconnectEvent(object sender, System.EventArgs e) {
        UpdateNetworkIndicators();
    }

    private void OnAnchorLoaded(object sender, System.EventArgs e)
    {
        FindObjectOfType<Anchor>().HideMesh();
        NetworkServer.Spawn(Instantiate(bridgeNetworkPrefab.gameObject));

        mainMenuUI.EnableGameStart();
        StartCoroutine(ResetPlayerCameraTransform());
    }

    private void OnCalibrationEnded(object sender, System.EventArgs e)
    {
        StartCoroutine(ResetPlayerCameraTransform());
    }



    private void HandleAnchorLoading() {
        if (PlayerPrefs.HasKey(Constants.anchorUuidPlayerPref))
        {
            anchorLoader.LoadAnchorsByUuid();   
        }
    }
    #endregion

    /**
     * Network indicators are updated via going through all
     * Network players in the scene and checking for VRplayer boolean.
     */
    private void UpdateNetworkIndicators()
    {
        bool VRActive = false;
        bool mobileActive = false;

        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            if (player.VRPlayer)
                VRActive = true;
            else
                mobileActive = true;
        }

        mainMenuUI.setVRIndicatorActive(VRActive);
        mainMenuUI.setMobileIndicatorActive(mobileActive);
    }

    /**  
     * Transform camera position and rotation in order for bridge anchor (and thus VR player) to face the UI. 
     * The transformations are based on the spatial anchor position.
     */
    IEnumerator ResetPlayerCameraTransform() {
        Anchor anchor = FindObjectOfType<Anchor>();
        if (anchor == null) { yield break; }

        OVRCameraRig ovrCamera = FindObjectOfType<OVRCameraRig>();

        ovrCamera.transform.Rotate(0, -anchor.transform.eulerAngles.y, 0);
        
        yield return new WaitForSecondsRealtime(.1f);

        ovrCamera.transform.position += new Vector3(-anchor.transform.position.x, 0, -anchor.transform.position.z);
    }
}
