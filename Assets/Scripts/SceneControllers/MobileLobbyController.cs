using Mirror;
using Mirror.Discovery;
using Oculus.Interaction.Samples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VrDashboardLogger.Editor;

public class MobileLobbyController : BaseController
{

    [SerializeField] private SceneLoader sceneLoader;

    [Header("UI")]
    [SerializeField] private ConnectScreenUI connectScreenUI;
    [SerializeField] private GameObject watchScreen;
    [SerializeField] private GameObject connectionErrorScreen;
    [SerializeField] private GameObject bridgeSettingButton;
    [SerializeField] private GameObject endExerciseButton;

    [Header("UI - No logging confirmation")]
    [SerializeField] private GameObject startWithoutLoggingDialog;
    [SerializeField] private Button startWithoutLoggingConfirmationButton;

    private void Start()
    {
        // Event handlers
        networkDiscovery.OnServerFound.AddListener(OnServerDiscovered);
        networkManager.OnClientDisconnectEvent += OnDisconnect;
        sceneLoader.OnSceneLoaded += OnSceneLoaded;
        sceneLoader.OnSceneUnloaded += OnSceneUnloaded;

        // Networking
        networkDiscovery.secretHandshake = 5630237;
        networkDiscovery.StartDiscovery();
        networkManager.Start();

        Debug.Log("Server discovery started");
    }

    private void OnSceneUnloaded(object sender, System.EventArgs e)
    {
        startWithoutLoggingConfirmationButton.onClick.RemoveAllListeners();
        startWithoutLoggingDialog.SetActive(false);

        watchScreen.SetActive(false);
        endExerciseButton.SetActive(false);
    }

    private void OnSceneLoaded(object sender, System.EventArgs e)
    {
        watchScreen.SetActive(true);
        endExerciseButton.SetActive(true);
    }

    private void OnServerDiscovered(ServerResponse info)
    {
        Debug.Log("Server Found " + info.uri);

        connectScreenUI.OnServerDiscovered(info);
    }

    private void OnDisconnect(object sender, System.EventArgs e)
    {
        connectScreenUI.OnDeviceDisconnected();
        bridgeSettingButton.SetActive(false);

        connectionErrorScreen.SetActive(true);

        // networkManager.StopClient();
        networkDiscovery.StartDiscovery();
    }


    public void Connect(ServerResponse info)
    {
        Debug.Log("Connecting " + info.uri);

        networkDiscovery.StopDiscovery();
        networkManager.StartClient(info.uri);

        VRState currentVRState = VRState.None;
        foreach (var player in FindObjectsOfType<NetworkPlayer>())
        {
            if (player.VRPlayer)
                currentVRState = player.VRState;
        }

        Debug.Log("CONNECT " + currentVRState);
        connectScreenUI.OnDeviceConnected(currentVRState);
    }

    public override void OnSceneSelected(string sceneName)
    {
        NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();

        if ((vrLogger == null || vrLogger.participantToLog.id == string.Empty) && !startWithoutLoggingDialog.activeSelf)
        {
            startWithoutLoggingDialog.SetActive(true);
            startWithoutLoggingConfirmationButton.onClick.AddListener(() => { OnSceneSelected(sceneName); });
        }
        else {
            startWithoutLoggingConfirmationButton.onClick.RemoveAllListeners();
            startWithoutLoggingDialog.SetActive(false);

            base.OnSceneSelected(sceneName);
        }
    }
}
