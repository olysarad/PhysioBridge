using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] Button startButton;

    [Header("Menu texts")]
    [SerializeField] TextMeshProUGUI infoText;
    [SerializeField] TextMeshProUGUI warningText;

    [Header("Network indicators")]
    [SerializeField] Image VRIndicator;
    [SerializeField] Image mobileIndicator;
    [SerializeField] private Color indicatorColorInactive;
    [SerializeField] private Color indicatorColorActive;


    public void DisableGameStart() 
    {
        startButton.enabled = false;
        startButton.gameObject.GetComponent<Image>().color = new Color(.3f, .3f, .3f, .5f);
        startButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = new Color(.5f, .5f, .5f, 1f);

        infoText.gameObject.SetActive(false);
        warningText.gameObject.SetActive(true);
    }

    public void EnableGameStart()
    {
        startButton.enabled = true;
        startButton.gameObject.GetComponent<Image>().color = new Color(0f, 0.6f, 1f, 1f);
        startButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = new Color(1f, 1f, 1f, 1f);

        infoText.gameObject.SetActive(true);
        warningText.gameObject.SetActive(false);
    }

    public void setVRIndicatorActive(bool value) {
        VRIndicator.color = value ? indicatorColorActive : indicatorColorInactive;
    }

    public void setMobileIndicatorActive(bool value)
    {
        mobileIndicator.color = value ? indicatorColorActive : indicatorColorInactive;
    }
}
