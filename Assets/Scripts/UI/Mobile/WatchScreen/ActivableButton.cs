using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivableButton : MonoBehaviour
{
    [SerializeField] private Sprite activeSprite;
    [SerializeField] private Sprite inactiveSprite;

    public void Activate(bool activate) {
        gameObject.GetComponent<Image>().sprite = activate ? activeSprite : inactiveSprite;
    }
}
