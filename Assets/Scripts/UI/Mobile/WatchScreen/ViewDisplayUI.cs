using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewDisplayUI : MonoBehaviour
{
    [SerializeField] public DisplayMode displayMode;

    [SerializeField] public ActivableButton activationButton;

    [SerializeField] private GameObject portraitDisplay;
    [SerializeField] private GameObject landscapeDisplay;

    public void UpdateScreenOrientation() {
        bool portraitOrientation = Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown;

        portraitDisplay.SetActive(portraitOrientation);
        landscapeDisplay.SetActive(!portraitOrientation);
    }
}
