using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DisplayMode
{
    PlayerCamera,
    TopCamera,
    Multiview,
    None
}
