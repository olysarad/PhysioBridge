using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Water;

public class WatchScreenUI : MonoBehaviour
{
    [SerializeField] private List<ViewDisplayUI> viewDisplays;
    [SerializeField] private List<ActivableButton> controlButtons;

    private ScreenOrientation currentOrientation;
    private ViewDisplayUI currentViewDisplay;

    private void Start()
    {
        currentOrientation = Screen.orientation;
        SetDisplayMode(DisplayMode.PlayerCamera.ToString());
    }

    private void Update()
    {
        if (Screen.orientation != currentOrientation) {
            currentOrientation = Screen.orientation;
            currentViewDisplay.UpdateScreenOrientation();
        }
    }

    private DisplayMode ParseDisplayMode(string displayMode)
    {
        switch (displayMode)
        {
            case "PlayerCamera":
                return DisplayMode.PlayerCamera;
            case "TopCamera":
                return DisplayMode.TopCamera;
            case "Multiview":
                return DisplayMode.Multiview;
            default:
                Debug.Log("Incorrect display mode");
                return DisplayMode.None;
        }
    }

    public void SetDisplayMode(string modeStr)
    {
        DisplayMode mode = ParseDisplayMode(modeStr);
        if (mode == DisplayMode.None) { return; }

        // Show View display
        viewDisplays.ForEach(display =>
        {
            display.gameObject.SetActive(display.displayMode == mode);

            if (display.displayMode == mode) { 
                currentViewDisplay = display;
                currentViewDisplay.UpdateScreenOrientation();    
            }
        });

        // Activate corresponding button
        controlButtons.ForEach(button =>
        {
            button.GetComponent<ActivableButton>().Activate(currentViewDisplay.activationButton == button);
        });
    }
}

