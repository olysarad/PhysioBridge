using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Controls : MonoBehaviour
{
    [SerializeField] public Toggle missingPlanks;
    [SerializeField] public TMP_Dropdown rugsPreview;
    [SerializeField] public TMP_Dropdown bridgeType;
}
