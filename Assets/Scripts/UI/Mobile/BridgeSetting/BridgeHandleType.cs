using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BridgeHandleType
{
    BothHandles,
    LeftHandle,
    RightHandle,
    NoHandle
}