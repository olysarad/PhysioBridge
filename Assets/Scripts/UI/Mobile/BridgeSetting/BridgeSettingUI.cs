using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;

public class BridgeSettingUI : MonoBehaviour
{
    [Header("Landscape mode")]
    [SerializeField] private GameObject landscapeSetting;
    [SerializeField] private BridgePreview landscapeBridgePreview;
    [SerializeField] private Controls landscapeControls;

    [Header("Portrait mode")]
    [SerializeField] private GameObject portraitSetting;
    [SerializeField] private BridgePreview portraitBridgePreview;
    [SerializeField] private Controls portraitControls;

    [Header("Controls values")]
    [SerializeField] private bool missingPlanks = false;
    [SerializeField] private RugsPreviewType rugsPreview = 0;
    [SerializeField] private BridgeHandleType handleType = 0;

    private ScreenOrientation currentOrientation;

    private void OnEnable()
    {
        Bridge bridge = FindObjectOfType<Bridge>();
        if (bridge == null) return;

        missingPlanks = bridge.missingPlanks;
        rugsPreview = bridge.rugsPreview;
        handleType = bridge.handleType;

        UpdateAll();
    }

    private void Start()
    {
        currentOrientation = Screen.orientation;
        UpdateScreenOrientation();
    }

    void Update()
    {
        if (Screen.orientation != currentOrientation)
        {
            currentOrientation = Screen.orientation;
            UpdateScreenOrientation();
        }
    }

    public void UpdateMissingPlanks(bool value)
    {
        Bridge bridge = FindObjectOfType<Bridge>();
        if(bridge != null)
            bridge.CmdSetHidePlanks(value);

        missingPlanks = value;
    }

    public void UpdateRugsPreview(int value)
    {
        Bridge bridge = FindObjectOfType<Bridge>();
        if (bridge != null)
            bridge.CmdSetRugsPreviewType((RugsPreviewType) value);

        rugsPreview = (RugsPreviewType) value;
    }

    public void UpdateBridgeType(int value)
    {
        Bridge bridge = FindObjectOfType<Bridge>();
        if (bridge != null)
            bridge.CmdSetHandleType((BridgeHandleType) value);

        handleType = (BridgeHandleType) value;
    }

    public void ResetSettings() {
        missingPlanks = false;
        rugsPreview = 0;
        handleType = 0;

        UpdateAll();
    }

    private void UpdateAll() {
        bool portraitOrientation = Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown;

        landscapeSetting.SetActive(true);
        UpdateControls(landscapeControls);
        landscapeBridgePreview.UpdateAll((int)rugsPreview, missingPlanks, (int)handleType);
        landscapeSetting.SetActive(!portraitOrientation);

        portraitSetting.SetActive(true);
        UpdateControls(portraitControls);
        portraitBridgePreview.UpdateAll((int)rugsPreview, missingPlanks, (int)handleType);
        portraitSetting.SetActive(portraitOrientation);
    }


    private void UpdateScreenOrientation()
    {
        bool portraitOrientation = Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown;

        portraitSetting.SetActive(portraitOrientation);
        landscapeSetting.SetActive(!portraitOrientation);

        if (portraitOrientation) {
            UpdateControls(portraitControls);
            portraitBridgePreview.UpdateMissingPlanks(missingPlanks);
            portraitBridgePreview.UpdateHandlesPreview((int) handleType);
        }
        else { 
            UpdateControls(landscapeControls);
            landscapeBridgePreview.UpdateMissingPlanks(missingPlanks);
            landscapeBridgePreview.UpdateHandlesPreview((int)handleType);
        }
    }

    private void UpdateControls(Controls controls) { 
        controls.missingPlanks.isOn = missingPlanks;
        controls.rugsPreview.value = (int) rugsPreview;
        controls.bridgeType.value = (int) handleType;
    }
}
