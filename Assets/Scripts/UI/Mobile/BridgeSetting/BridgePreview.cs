﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class BridgePreview : MonoBehaviour
{
    [Header("GameObjects")]
    [SerializeField] private GameObject bridge;
    [SerializeField] private GameObject handles;

    [Header("Missing planks")]
    [SerializeField] private Sprite allPlanks;
    [SerializeField] private Sprite missingPlanks;

    [Header("Bridge types")]
    [SerializeField] private Sprite bothHandles;
    [SerializeField] private Sprite leftHandle;
    [SerializeField] private Sprite rightHandle;

    [Header("Rug switching")]
    [SerializeField] public GameObject rugs;
    [SerializeField] public GameObject firstSwitchRug;
    [SerializeField] private int firstSwitchRugIndex;
    [SerializeField] public GameObject secondSwitchRug;
    [SerializeField] private int secondSwitchRugIndex;


    private void SwitchRugs(bool _switch)
    {
        if (_switch)
        {
            firstSwitchRug.transform.SetSiblingIndex(secondSwitchRugIndex);
            secondSwitchRug.transform.SetSiblingIndex(firstSwitchRugIndex);
        }
        else {
            firstSwitchRug.transform.SetSiblingIndex(firstSwitchRugIndex);
            secondSwitchRug.transform.SetSiblingIndex(secondSwitchRugIndex);
        }
        
    }

    public void UpdateHandlesPreview(int value)
    {
        if (value > System.Enum.GetValues(typeof(BridgeHandleType)).Length)
        {
            Debug.Log("ERR: Incorrect bridge type");
            return;
        }

        Image previewImage = bridge.GetComponent<Image>();
        BridgeHandleType bridgeType = (BridgeHandleType)value;

        switch (bridgeType)
        {
            case BridgeHandleType.BothHandles:
                previewImage.sprite = bothHandles;
                break;
            case BridgeHandleType.LeftHandle:
                previewImage.sprite = leftHandle;
                break;
            case BridgeHandleType.RightHandle:
                previewImage.sprite = rightHandle;
                break;
            case BridgeHandleType.NoHandle:
                previewImage.sprite = null;
                break;
        }
    }

    public void UpdateRugsPreview(int value) {
        if (value > System.Enum.GetValues(typeof(RugsPreviewType)).Length)
        {
            Debug.Log("ERR: Incorrect rugs preview option");
            return;
        }

        RugsPreviewType rugsType = (RugsPreviewType) value;

        switch (rugsType)
        {
            case RugsPreviewType.DefaultRugs:
                SwitchRugs(false);
                rugs.SetActive(true);
                break;
            case RugsPreviewType.SwitchedRugs:
                SwitchRugs(true);
                rugs.SetActive(true);
                break;
            case RugsPreviewType.NoRugs:
                rugs.SetActive(false);
                break;
        }
    }

    public void UpdateMissingPlanks(bool missing)
    {
        Image previewImage = handles.GetComponent<Image>();
        if (missing)
            previewImage.sprite = missingPlanks;
        else
            previewImage.sprite = allPlanks;
    }

    public void UpdateAll(int _switch, bool hide, int handleType) {
        UpdateRugsPreview(_switch);
        UpdateMissingPlanks(hide);
        UpdateHandlesPreview(handleType);
    }
}
