using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VrDashboardLogger.Editor;
using VrDashboardLogger.Editor.Classes;

public class VrLoggingUI : MonoBehaviour
{
    [Header("Texts")]
    [SerializeField] private TextMeshProUGUI loggingEnabledText;
    [SerializeField] private TextMeshProUGUI loggingDisabledText;

    [Header("Buttons")]
    [SerializeField] private Button enableButton;
    [SerializeField] private Button disableButton;

    [Header("Dropdown")]
    [SerializeField] private TMP_Dropdown participantsDropdown;

    private Participant emptyParticipant = new Participant();

    private void Start()
    {
        emptyParticipant.id = string.Empty;
    }

    private void OnEnable()
    {
        NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();
        if (vrLogger != null)
        {
            ShowParticipants(vrLogger.participants);
        }
    }

    public void ShowParticipants(List<Participant> participants)
    {
        List<string> dropdownOptions = new List<string> {" "};
        participants.ForEach(p => { dropdownOptions.Add(p.nickname); });

        participantsDropdown.ClearOptions();
        participantsDropdown.AddOptions(dropdownOptions);
    }

    public void OnParticipantPicked(int participantIdx) {
        enableButton.interactable = (participantIdx != 0);
    }

    public void UpdateUIBasedOnServerData()
    {
        NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();
        if (vrLogger == null)
        {
            Debug.Log("WARNING: no Vr logger found");
            return;
        }

        Participant participant = vrLogger.participantToLog;
        Debug.Log("Participant from server " + participant);
        if (participant.id == string.Empty)
        {
            UpdateUIComponent(false);
        }
        else {
            UpdateUIComponent(true);

            // Set correct option in dropdown based on participant name
            int idx = 0;
            participantsDropdown.options.ForEach(option =>
            {
                if (option.text == participant.nickname) {
                    participantsDropdown.SetValueWithoutNotify(idx);
                    OnParticipantPicked(idx);
                    return;
                }
                idx += 1;
            });
        }
    }

    public void ActivateLogging(bool activateLogging) {
        NetworkVrLogger vrLogger = FindObjectOfType<NetworkVrLogger>();
        if (vrLogger == null) {
            Debug.Log("WARNING: no Vr logger found");
            return;
        }

        // Update the UI of logger panel
        UpdateUIComponent(activateLogging);

        // Update Participant to log on the server
        if (!activateLogging)
        {
            vrLogger.CmdSetParticipantToLog(emptyParticipant);
        }
        else
        {   // Find participant by nickname from dropdown
            vrLogger.participants.ForEach(p => {
                if (p.nickname == participantsDropdown.options[participantsDropdown.value].text)
                {
                    vrLogger.CmdSetParticipantToLog(p);
                }
            });
        }
    }

    private void UpdateUIComponent(bool activateLogging) {
        loggingEnabledText.gameObject.SetActive(activateLogging);
        loggingDisabledText.gameObject.SetActive(!activateLogging);

        enableButton.gameObject.SetActive(!activateLogging);
        disableButton.gameObject.SetActive(activateLogging);

        participantsDropdown.interactable = !activateLogging;
    }
}
