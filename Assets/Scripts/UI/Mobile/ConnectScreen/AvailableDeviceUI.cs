using Mirror.Discovery;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class AvailableDeviceUI : MonoBehaviour
{
    [Header("Device info")]
    [SerializeField] private TextMeshProUGUI deviceNameText;
    [SerializeField] private TextMeshProUGUI deviceNameNotFoundText;

    [Header("Join button")]
    [SerializeField] private Button joinButton;
    [SerializeField] private TextMeshProUGUI connectedText;

    [Header("Availability indicator")]
    [SerializeField] private Image availabilityIndicator;
    [SerializeField] private Color indicatorColorInactive;
    [SerializeField] private Color indicatorColorActive;

    private ServerResponse availableServerResponse;

    private void Start()
    {
        availabilityIndicator.color = indicatorColorInactive;
    }

    public void ShowAvailableDevice(ServerResponse serverResponse)
    {
        availableServerResponse = serverResponse;

        availabilityIndicator.color = indicatorColorActive;

        deviceNameText.text = serverResponse.EndPoint.Address.ToString();
        deviceNameText.gameObject.SetActive(true);
        deviceNameNotFoundText.gameObject.SetActive(false);
        
        joinButton.interactable = true;
        joinButton.onClick.AddListener(Connect);
    }

    public void ResetAvailableDevice()
    {
        availabilityIndicator.color = indicatorColorInactive;

        deviceNameText.gameObject.SetActive(false);
        deviceNameNotFoundText.gameObject.SetActive(true);

        joinButton.interactable = false;
        joinButton.gameObject.SetActive(true);
        connectedText.gameObject.SetActive(false);
    }

    public void ShowConnectedStatus() { 
        joinButton.interactable = false;
        joinButton.gameObject.SetActive(false);

        connectedText.gameObject.SetActive(true);
    }

    private void Connect()
    {
        FindObjectOfType<MobileLobbyController>().Connect(availableServerResponse);
    }
}
