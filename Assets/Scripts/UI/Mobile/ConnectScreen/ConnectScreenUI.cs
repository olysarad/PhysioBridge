using Mirror.Discovery;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using VrDashboardLogger.Editor;

public class ConnectScreenUI : MonoBehaviour
{

    [SerializeField] private GameObject sceneSelection;

    [SerializeField] private AvailableDeviceUI availableDeviceUI;
    [SerializeField] private VrLoggingUI vrLoggerUI;

    [Header("Spinner elements")]
    [SerializeField] private GameObject searchingForDevices;
    [SerializeField] private GameObject calibrationInProgress;

    [Header("Info elements")]
    [SerializeField] private GameObject hintBar;
    [SerializeField] private GameObject runningScene;
    [SerializeField] private GameObject calibrationNeeded;

    public void OnServerDiscovered(ServerResponse serverResponse)
    {
        availableDeviceUI.ShowAvailableDevice(serverResponse);
    }

    public void OnDeviceConnected(VRState currentVRState) {
        OnVRStateChange(currentVRState);

        hintBar.SetActive(false);
        searchingForDevices.SetActive(false);

        availableDeviceUI.ShowConnectedStatus();
    }

    public void OnDeviceDisconnected()
    {
        vrLoggerUI.gameObject.SetActive(false);
        sceneSelection.SetActive(false);
        runningScene.SetActive(false);
        calibrationNeeded.SetActive(false);

        searchingForDevices.SetActive(true);

        availableDeviceUI.ResetAvailableDevice();
    }

    public void OnVRStateChange(VRState state) { 
        calibrationInProgress.SetActive(state == VRState.Calibration);

        sceneSelection.SetActive(state == VRState.LobbyWithAnchor);
        vrLoggerUI.gameObject.SetActive(state == VRState.LobbyWithAnchor);

        runningScene.SetActive(state == VRState.Exercise);

        calibrationNeeded.SetActive(state == VRState.LobbyWithoutAnchor);
    }
}
