using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileLobbyUI : MonoBehaviour
{
    [SerializeField] private ConnectScreenUI connectScreenUI;
    [SerializeField] private BridgeSettingUI bridgeSettingUI;

    [SerializeField] private GameObject bridgeSettingButton;

    public void OnVRStateChange(VRState state)
    {
        connectScreenUI.OnVRStateChange(state);

        if (state == VRState.Calibration) {
            bridgeSettingUI.gameObject.SetActive(true);
            bridgeSettingUI.ResetSettings();
            bridgeSettingUI.gameObject.SetActive(false);
        }

        bridgeSettingButton.SetActive(state == VRState.LobbyWithAnchor);    //  || state == VRState.Exercise ? 
    }
}
